﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace some_app
{
    public partial class MainPage : ContentPage
    {
        private int buttonClickCount = 0;

        public MainPage()
        {

            InitializeComponent();
            var myButton = new Button
            {
                Text = "Моя кнопка"
            };
            myButton.Clicked += Button_Clicked;

            Content = new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Children = { myButton }
            };
        }

        private void Button_Clicked(object sender, System.EventArgs e)
        {
            buttonClickCount++;
            DisplayAlert("Количество нажатий", $"Кнопка была нажата {buttonClickCount} раз(а).", "ОК");
        }


    }
}
